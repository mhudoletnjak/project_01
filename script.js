'use strict';


//LOGIKA IGRICE
let number = Math.trunc(Math.random() * 20) + 1;
let highscore=0;
let score = 20;
const displayMess=function(message){
  document.querySelector(".message").textContent=message;
}


document.querySelector('.check').addEventListener('click', function(){
  const guess=Number( document.querySelector('.guess').value);

  if(!guess){//Nije nista stavio
    //document.querySelector(".message").textContent="No Number!";
    displayMess("No number!")
  }else if(guess === number){//Pobjeda
    document.querySelector(".number").textContent=number;//Prikazi skriveni broj
    displayMess("Correct Number!");
    
    //Dogadaj nakon pobjede
    document.querySelector("body").style.backgroundColor = "#60b347";
    document.querySelector(".number").style.width = "30rem"
    if(score>highscore){
      highscore=score;
      document.querySelector(".highscore").textContent=highscore;
    }

  }else if(guess!==number){
    if(score>1){
      displayMess(guess>number ? 'Too High' : 'Too Low');
      score--;
      document.querySelector(".score").textContent=score;
      }else{
        displayMess("You lost the game");
        document.querySelector(".score").textContent=0;
  }
}
});
//Again
document.querySelector(".again").addEventListener("click",function(){
  score=20;
  number = Math.trunc(Math.random() * 20) + 1;
  displayMess("Start guessing ...");
  document.querySelector(".score").textContent=score;
  document.querySelector(".number").textContent="?";
  document.querySelector("body").style.backgroundColor="#222";
  document.querySelector(".number").style.width="15rem";
  document.querySelector(".guess").value="";  
});

